# changelog-manager

<a href="https://asciinema.org/a/178306" title="changelog-manager screencast"><img alt="asciinema" src="changelog-asciinema.png"></a>

## TL;DR

- Which problems does this tool solve ➡️ Creating changelog entries as soon and simple as possible (when writing code) to avoid doing code/git archaeology when you need to release
- Why not writing directly to CHANGELOG.md ➡️ Because you'll get merge conflicts
- Why not using an existing tool ➡️ Most tools parse title from git commit messages, which are not suitable for the customers' release notes
- What is making this tool so special ➡️ YAML files are super easy to write (or generated) and maintain & CHANGELOG.md is automatically generated and sorted

<hr>

This project is heavily inspired by [GitLab's Changelog development guide](https://docs.gitlab.com/ee/development/changelog.html).

In comparision to GitLab this project provides a standalone CLI which supports both steps:

- generating YAML files (changelog entries)
- aggregating YAML files to a CHANGELOG.md

You can customize these things:

- types (added, fixed, ...)
- directories and files names (changeslogs/unreleased, CHANGELOG.md)
- extends proprties in interactive CLI
- customizable template for changelog entries in CHANGELOG.md

## Requirements

- Node.js (6 and above)
- npm

Download [from official website](https://nodejs.org/en/). 
or use [n version manager](https://github.com/tj/n) for Node.js via `curl -L https://git.io/n-install | bash`


## Install

```
npm install changelog-manager -g
```

You can also install it without `-g` but then you need to put the binary,
located in `node_modules/.bin/changelog-manager` to your `$PATH`.

## Usage

The CLI provides 3 subcommands

### `add`
Create an changelog entry (YAML file) in `changelogs/unreleased`

### `release`
Aggregate files in `changelogs/unreleased` and transform them into a markdown file (CHANGELOGS.md)

### `strip-link`
If you have some links to your code (merge requests) in your CHANGELOG.md but your customers won't be able to open that (because your code is not open sources, etc.) you can remove those links wich this subcommand.

---

You can just invoke `changelog-manager` (or pass `--help`) to show help output.

For help output of a subcommand use `-h` instead!

### Docker image

Coming soon

### Full features description

Coming soon
